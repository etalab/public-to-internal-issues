# Public to internal issues

Synchronize issues from public project etalab/suivi to internal project etalab/suivi-interne.

## Install

```bash
git clone https://git.framasoft.org/etalab/public-to-internal-issues.git
cd public-to-internal-issues
npm install
```

## Usage

```bash
node suivi2interne.js ${FRAMAGIT_PRIVATE_TOKEN}
```
