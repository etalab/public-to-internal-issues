// public-to-internal-issues -- Synchronize GitLab public issues with internal ones
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2015 Etalab
// https://git.framasoft.org/etalab/public-to-internal-issues
//
// public-to-internal-issues is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// public-to-internal-issues is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import fetch from "node-fetch"
import querystring from "querystring"
import url from "url"


export async function createItem(context, path, item) {
  return await sendItem(context, path, item, "POST")
}


export async function fetchItem(context, path) {
  const res = await fetch(url.resolve(context.baseUrl, path), {headers: {"PRIVATE-TOKEN": context.privateToken}})
  return await res.json()
}


export async function fetchItems(context, path) {
  let items = []
  for (let pageNumber = 1;; pageNumber++){
    const query = querystring.stringify({page: pageNumber})
    const res = await fetch(url.resolve(context.baseUrl, `${path}?${query}`), {
      headers: {"PRIVATE-TOKEN": context.privateToken},
    })
    const pageItems = await res.json()
    if (pageItems.length === 0) break
    items = [...items, ...pageItems]
    if (!res.headers.get("link")) break
  }
  return items
}


async function sendItem(context, path, item, method) {
  const body = JSON.stringify(item, null, 2)
  const res = await fetch(url.resolve(context.baseUrl, path), {
    body,
    headers: {
      "Content-Length": new Buffer(body).length.toString(),
      "Content-Type": "application/json; charset=utf-8",
      "PRIVATE-TOKEN": context.privateToken,
    },
    method,
  })
  return await res.json()
}


export async function updateItem(context, path, item) {
  return await sendItem(context, path, item, "PUT")
}
