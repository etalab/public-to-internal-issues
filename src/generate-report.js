// public-to-internal-issues -- Synchronize GitLab public issues with internal ones
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2015 Etalab
// https://git.framasoft.org/etalab/public-to-internal-issues
//
// public-to-internal-issues is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// public-to-internal-issues is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import assert from "assert"
import escape from "escape-html"
import marked from "marked"
import fs from "mz/fs"
import path from "path"

import {fetchItem, fetchItems} from "./fetchers"
import {iterChapters, iterCommitments, iterSubChapters, parseSummary} from "./gitbook"


function extractIssuesIid(fragment) {
  const issueLinkRe = /_\[\[suivi\]\(https:\/\/git\.framasoft\.org\/etalab\/suivi\/issues\/(\d+)\)\]_/g
  let issuesNumber = []
  let match
  while ((match = issueLinkRe.exec(fragment)) !== null) {
    issuesNumber.push(parseInt(match[1]))
  }
  return issuesNumber
}


async function main() {
  const privateToken = process.argv[2]
  assert(privateToken, "Missing argument for FramaGit private token.")
  const reportDir = process.argv[3]
  assert(reportDir, "Missing argument for path of report directory.")

  const context = {
    baseUrl: "https://git.framasoft.org/api/v3/",
    privateToken,
  }
  const summaryPath = "../plan-ogp-2015-2017/fr/SUMMARY.md"
  const summary = await parseSummary(summaryPath)

  const internalProjectPath = "etalab/suivi-interne"
  const internalProject = await fetchItem(context, `projects/${encodeURIComponent(internalProjectPath)}`)

  let commitmentIssuesByNumber = {}
  for (let issue of await fetchItems(context, `projects/${internalProject.id}/issues`)) {
    if (!issue.labels) continue
    let commitmentNumber = null
    for (let label of issue.labels) {
      let match = label.match(/^Engagement (\d+)/)
      if (match) {
        commitmentNumber = match[1]
        break
      }
    }
    if (!commitmentNumber) continue
    let commitmentIssues = commitmentIssuesByNumber[commitmentNumber]
    if (!commitmentIssues) commitmentIssuesByNumber[commitmentNumber] = commitmentIssues = []
    commitmentIssues.push(issue)

    let match = issue.description.trim().match(/^Ticket public etalab\/suivi#(\d+)/)
    issue.publicIid = parseInt(match[1])
  }

  let recapNoteByIssueId = {}
  for (let commitmentNumber in commitmentIssuesByNumber) {
    let commitmentIssues = commitmentIssuesByNumber[commitmentNumber]
    for (let issue of commitmentIssues) {
      for (let note of await fetchItems(context, `projects/${internalProject.id}/issues/${issue.id}/notes`)) {
        if (note.system) continue
        if (note.body.match(/^#+ Récapitulatif/)) {
          recapNoteByIssueId[issue.id] = note
          break
        }
      }
    }
  }

  let chapters = []
  let institutions = []
  const institutionsRe = /\*\*Institutions? porteuses?/gi
  const roadmapRe = /###+\s+Feuille de route/gi
  for (let chapter of iterChapters(summary)) {
    chapters.push(chapter)
    chapter.subChapters = []
    for (let subChapter of iterSubChapters(chapter)) {
      chapter.subChapters.push(subChapter)
      subChapter.commitments = []
      for (let commitment of iterCommitments(subChapter)) {
        subChapter.commitments.push(commitment)
        commitment.number = commitment.title.match(/^Engagement (\d+)/)[1]
        let commitmentPath = path.join(path.dirname(summaryPath), commitment.path)
        commitment.text = await fs.readFile(commitmentPath, "utf-8")

        commitment.institutions = []
        while (institutionsRe.exec(commitment.text) !== null) {
          let firstLine = true
          for (let line of commitment.text.substring(institutionsRe.lastIndex).split("\n")) {
            if (firstLine) {
              firstLine = false
              continue
            }
            line = line.trim()
            if (!line) break
            if (line.startsWith("-")) line = line.substring(1)
            if (line.endsWith(";")) line = line.substring(0, line.length - 1)
            let institution = line.trim()
            if (!commitment.institutions.includes(institution)) {
              commitment.institutions.push(institution)
              if (!institutions.includes(institution)) institutions.push(institution)
            }
          }
        }

        commitment.issues = commitmentIssuesByNumber[commitment.number] || []

        let roadmapItems = []
        let roadmapIssuesIid = []
        while (roadmapRe.exec(commitment.text) !== null) {
          let firstLine = true
          let inRoadmapItem = false
          for (let line of commitment.text.substring(roadmapRe.lastIndex).split("\n")) {
            if (firstLine) {
              firstLine = false
              continue
            }
            if (line.startsWith(" ")) {
              line = line.trim()
              if (line.startsWith("-") || line.startsWith("*")) inRoadmapItem = false
              let itemIndex = roadmapItems.length - 1
              if (inRoadmapItem) roadmapItems[itemIndex] = roadmapItems[itemIndex] + "\n" + line
              Array.prototype.push.apply(roadmapIssuesIid[itemIndex], extractIssuesIid(line))
              continue
            }
            if (line.startsWith("--") || line.startsWith("#")) break
            // if (!line.startsWith("- ") && line.trim()) break
            if (line.startsWith("- ")) {
              inRoadmapItem = true
              let roadmapItem = line.substring(2).trim()
              if (roadmapItem) {
                roadmapItems.push(roadmapItem)
                roadmapIssuesIid.push(extractIssuesIid(roadmapItem))
              }
            } else {
              let itemIndex = roadmapItems.length - 1
              if (inRoadmapItem) roadmapItems[itemIndex] = roadmapItems[itemIndex] + "\n" + line
              if (itemIndex > 0) Array.prototype.push.apply(roadmapIssuesIid[itemIndex], extractIssuesIid(line))
            }
          }
        }
        commitment.roadmapIssuesIid = roadmapIssuesIid
        commitment.roadmapItems = [
          for (roadmapItem of roadmapItems)
          roadmapItem = roadmapItem
            .replace(/\s*_\[\[suivi\].*?\]_\s*/g, " ")  // Remove links to issues
            .replace(/\[\^\d+\]/g, "")  // Remove footnotes.

        ]
      }
    }
  }
  commitmentIssuesByNumber = null  // Don't use it anymore.

  let outputFile

  if (!await fs.exists(reportDir)) await fs.mkdir(reportDir)
  outputFile = await fs.open(path.join(reportDir, "index.html"), "w")
  await renderIndexPage(outputFile, chapters, recapNoteByIssueId)
  fs.close(outputFile)

  outputFile = await fs.open(path.join(reportDir, "institutions.html"), "w")
  await renderInstitutionsPage(outputFile, institutions)
  fs.close(outputFile)

  for (let institution of institutions) {
    outputFile = await fs.open(path.join(reportDir, `${institution}.html`), "w")
    await renderInstitutionPage(outputFile, institution, chapters, recapNoteByIssueId)
    fs.close(outputFile)
  }
}


async function renderFooter(outputFile) {
  await fs.write(outputFile, `\
    <footer class="white bg-black">
      <div class="px2 py3 container">
        <p class="h6 mb0">© 2015, Etalab</p>
      </div>
    </footer>
  </body>
</html>\n`)
}


async function renderIndexPage(outputFile, chapters, recapNoteByIssueId) {
  await fs.write(outputFile, `<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Avancement global Plan PGO 2015-2017</title>
    <meta name="description" content="Avancement du plan 2015-2017 du partenariat pour un gouvernement ouvert">
    <meta name="author" content="Etalab">
    <meta name="keywords" content="gouvernement ouvert ogp open opengov">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="http://d2v52k3cl9vedd.cloudfront.net/basscss/7.0.4/basscss.min.css" rel="stylesheet">
  </head>
  <body>
    <nav class="clearfix white bg-black">
      <div class="sm-col">
        <span class="btn py2">Sommaire</span>
        <a href="institutions.html" class="btn py2">Institutions porteuses</a>
        <!-- a href="#" class="btn py2">Avancement</a -->
      </div>
      <!-- div class="sm-col-right">
        <a href="#" class="btn py2">À propos</a>
      </div -->
    </nav>
    <h1>Avancement Plan PGO 2015-2017</h1>\n`)
  for (let chapter of chapters) {
    await fs.write(outputFile, `\
    <section class="border bg-silver">
      <h2>${escape(chapter.title)}</h2>\n`)
    for (let subChapter of chapter.subChapters) {
      if (subChapter.title) {
        await fs.write(outputFile, `\
      <section class="p1">
        <h3>${escape(subChapter.title)}</h3>\n`)
      }
      for (let commitment of subChapter.commitments) {
        await fs.write(outputFile, `\
        <article class="p1 border bg-white">
          <header class="p1 white bg-blue rounded">
            <h4>${escape(commitment.title)}</h4>
          </header>\n`)
        await fs.write(outputFile, `\
          <div>
            <h5>Institutions porteuses</h5>
            <ul>\n`)
        for (let commitmentInstitution of commitment.institutions) {
          await fs.write(outputFile, `\
              <li><a href="${commitmentInstitution}.html">${escape(commitmentInstitution)}</a></li>\n`)
        }
        await fs.write(outputFile, `\
            </ul>
          </div>\n`)

        let encounterdIssuesIid = []
        for (let [roadmapItem, roadmapItemIssuesIid] of zip(commitment.roadmapItems, commitment.roadmapIssuesIid)) {
          await fs.write(outputFile, `\
          <section class="clearfix p1 border">
            <div class="lg-col lg-col-4">
              <h5 class="gray">Feuille de route</h5>
              ${marked(roadmapItem)}
            </div>\n`)

          await fs.write(outputFile, `\
            <div class="lg-col lg-col-8">\n`)
          for (let issue of commitment.issues) {
            if (roadmapItemIssuesIid.includes(issue.publicIid)) {
              encounterdIssuesIid.push(issue.publicIid)
              await renderIssue(outputFile, issue, recapNoteByIssueId[issue.id])
            }
          }
          await fs.write(outputFile, `\
            </div>\n`)

          await fs.write(outputFile, `\
          </section>\n`)
        }
        let remainingIssues = []
        for (let issue of commitment.issues) {
          if (!encounterdIssuesIid.includes(issue.publicIid)) remainingIssues.push(issue)
        }
        if (remainingIssues.length > 0) {
          await fs.write(outputFile, `\
          <section class="p1 border">
            <h5 class="gray">Et aussi...</h5>\n`)

          for (let issue of remainingIssues) {
            await renderIssue(outputFile, issue, recapNoteByIssueId[issue.id])
          }

          await fs.write(outputFile, `\
          </section>\n`)
        }

        await fs.write(outputFile, `\
        </article>\n`)
      }
      await fs.write(outputFile, `\
      </section>\n`)
    }
    await fs.write(outputFile, `\
    </section>\n`)
  }
  await renderFooter(outputFile)
}


async function renderInstitutionPage(outputFile, institution, chapters, recapNoteByIssueId) {
  await fs.write(outputFile, `<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Avancement Plan PGO 2015-2017</title>
    <meta name="description" content="Avancement du plan 2015-2017 du partenariat pour un gouvernement ouvert">
    <meta name="author" content="Etalab">
    <meta name="keywords" content="gouvernement ouvert ogp open opengov">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="http://d2v52k3cl9vedd.cloudfront.net/basscss/7.0.4/basscss.min.css" rel="stylesheet">
  </head>
  <body>
    <nav class="clearfix white bg-black">
      <div class="sm-col">
        <a href="index.html" class="btn py2">Sommaire</a>
        <a href="institutions.html" class="btn py2">Institutions porteuses</a>
        <!-- a href="#" class="btn py2">Avancement</a -->
      </div>
      <!-- div class="sm-col-right">
        <a href="#" class="btn py2">À propos</a>
      </div -->
    </nav>
    <h1>Avancement Plan PGO 2015-2017 pour ${institution}</h1>\n`)
  for (let chapter of chapters) {
    let institutionFound = false
    for (let subChapter of chapter.subChapters) {
      for (let commitment of subChapter.commitments) {
        if (commitment.institutions.includes(institution)) {
          institutionFound = true
          break
        }
      }
      if (institutionFound) break
    }
    if (!institutionFound) continue

    await fs.write(outputFile, `\
    <section class="border bg-silver">
      <h2>${escape(chapter.title)}</h2>\n`)
    for (let subChapter of chapter.subChapters) {
      institutionFound = false
      for (let commitment of subChapter.commitments) {
        if (commitment.institutions.includes(institution)) {
          institutionFound = true
          break
        }
      }
      if (!institutionFound) continue

      if (subChapter.title) {
        await fs.write(outputFile, `\
      <section class="p1">
        <h3>${escape(subChapter.title)}</h3>\n`)
      }
      for (let commitment of subChapter.commitments) {
        if (!commitment.institutions.includes(institution)) continue
        await fs.write(outputFile, `\
        <article class="p1 border bg-white">
          <header class="p1 white bg-blue rounded">
            <h4>${escape(commitment.title)}</h4>
          </header>\n`)
        await fs.write(outputFile, `\
          <div>
            <h5>Institutions porteuses</h5>
            <ul>\n`)
        for (let commitmentInstitution of commitment.institutions) {
          if (commitmentInstitution === institution)
            await fs.write(outputFile, `\
                <li>${escape(commitmentInstitution)}</li>\n`)
          else
            await fs.write(outputFile, `\
                <li><a href="${commitmentInstitution}.html">${escape(commitmentInstitution)}</a></li>\n`)
        }
        await fs.write(outputFile, `\
            </ul>
          </div>\n`)

        let encounterdIssuesIid = []
        for (let [roadmapItem, roadmapItemIssuesIid] of zip(commitment.roadmapItems, commitment.roadmapIssuesIid)) {
          await fs.write(outputFile, `\
          <section class="clearfix p1 border">
            <div class="lg-col lg-col-4">
              <h5 class="gray">Feuille de route</h5>
              ${marked(roadmapItem)}
            </div>\n`)

          await fs.write(outputFile, `\
            <div class="lg-col lg-col-8">\n`)
          for (let issue of commitment.issues) {
            if (roadmapItemIssuesIid.includes(issue.publicIid)) {
              encounterdIssuesIid.push(issue.publicIid)
              await renderIssue(outputFile, issue, recapNoteByIssueId[issue.id])
            }
          }
          await fs.write(outputFile, `\
            </div>\n`)

          await fs.write(outputFile, `\
          </section>\n`)
        }
        let remainingIssues = []
        for (let issue of commitment.issues) {
          if (!encounterdIssuesIid.includes(issue.publicIid)) remainingIssues.push(issue)
        }
        if (remainingIssues.length > 0) {
          await fs.write(outputFile, `\
          <section class="p1 border">
            <h5 class="gray">Et aussi...</h5>\n`)

          for (let issue of remainingIssues) {
            await renderIssue(outputFile, issue, recapNoteByIssueId[issue.id])
          }

          await fs.write(outputFile, `\
          </section>\n`)
        }

        await fs.write(outputFile, `\
        </article>\n`)
      }
      await fs.write(outputFile, `\
      </section>\n`)
    }
    await fs.write(outputFile, `\
    </section>\n`)
  }
  await renderFooter(outputFile)
}


async function renderInstitutionsPage(outputFile, institutions) {
  await fs.write(outputFile, `<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Avancement Plan PGO 2015-2017</title>
    <meta name="description" content="Avancement du plan 2015-2017 du partenariat pour un gouvernement ouvert">
    <meta name="author" content="Etalab">
    <meta name="keywords" content="gouvernement ouvert ogp open opengov">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="http://d2v52k3cl9vedd.cloudfront.net/basscss/7.0.4/basscss.min.css" rel="stylesheet">
  </head>
  <body>
    <nav class="clearfix white bg-black">
      <div class="sm-col">
        <a href="index.html" class="btn py2">Sommaire</a>
        <span class="btn py2">Institutions porteuses</span>
        <!-- a href="#" class="btn py2">Avancement</a -->
      </div>
      <!-- div class="sm-col-right">
        <a href="#" class="btn py2">À propos</a>
      </div -->
    </nav>
    <h1>Institutions porteuses du Plan PGO 2015-2017</h1>
    <ul>\n`)
  for (let institution of institutions) {
    await fs.write(outputFile, `\
      <li><a href="${institution}.html">${escape(institution)}</a></li>\n`)
  }
    await fs.write(outputFile, `\
    </ul>\n`)
  await renderFooter(outputFile)
}


async function renderIssue(outputFile, issue, recapNote) {
  const issueUrl = `https://git.framasoft.org/etalab/suivi/issues/${issue.publicIid}`
  await fs.write(outputFile, `\
            <section class="clearfix p1 border">
              <div class="sm-col sm-col-6">
                <h6>
                  <a href="${escape(issueUrl)}" target="_blank">${escape(issue.title)}</a>
                </h6>\n`)
  if (issue.description) {
    let description = issue.description.trim()
      .replace(/^Ticket public etalab\/suivi.*$/im, "")
    if (description) {
      await fs.write(outputFile, `\
                ${marked(description)}\n`)
    }
  }
      await fs.write(outputFile, `\
              </div>\n`)
  await fs.write(outputFile, `\
              <section class="sm-col sm-col-6 px1">
                <h6 class="inline-block px1 white bg-red rounded">Non démarré</h6>\n`)
  if (recapNote) {
    let recap = recapNote.body.replace(/^#+ Récapitulatif\s*/u, "")
    await fs.write(outputFile, `\
                <div class="bg-silver">
                  ${marked(recap)}
                </div>\n`)
  }
  await fs.write(outputFile, `\
              </section>\n`)
  await fs.write(outputFile, `\
            </section>\n`)
}


function zip(...arrays) {
  return arrays[0].map((_, i) => arrays.map(array => array[i]))
}


main()
  .catch(error => console.log(error.stack))
