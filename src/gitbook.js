// public-to-internal-issues -- Synchronize GitLab public issues with internal ones
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2015 Etalab
// https://git.framasoft.org/etalab/public-to-internal-issues
//
// public-to-internal-issues is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// public-to-internal-issues is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gitbookParsers from "gitbook-parsers"
import fs from "mz/fs"


export function* iterChapters(summary) {
  for (let chapter of summary.chapters) {
    if (chapter.articles.length === 0) continue
    yield chapter
  }
}


export function* iterCommitments(subChapter) {
  yield* subChapter.articles
}


export function* iterSubChapters(chapter) {
  if (chapter.articles[0].articles.length === 0) {
    let subChapter = {...chapter}
    delete subChapter.title
    yield subChapter
  } else yield* chapter.articles
}


export async function parseSummary(summaryPath) {
  const summaryText = await fs.readFile(summaryPath, "utf-8")
  const parser = gitbookParsers.getForFile(summaryPath)
  return await parser.summary(summaryText)
}
