// public-to-internal-issues -- Synchronize GitLab public issues with internal ones
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2015 Etalab
// https://git.framasoft.org/etalab/public-to-internal-issues
//
// public-to-internal-issues is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// public-to-internal-issues is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import assert from "assert"
import lodash from "lodash"

import {createItem, fetchItem, fetchItems, updateItem} from "./fetchers"


function cleanupItem(item, ignore) {
  item = {...item}
  for (let name of ignore) delete item[name]
  return item
}


async function main() {
  const privateToken = process.argv[2]
  assert(privateToken, "Missing argument for FramaGit private token.")
  const context = {
    baseUrl: "https://git.framasoft.org/api/v3/",
    privateToken,
  }
  const internalProjectPath = "etalab/suivi-interne"
  const publicProjectPath = "etalab/suivi"
  const internalProject = await fetchItem(context, `projects/${encodeURIComponent(internalProjectPath)}`)
  const publicProject = await fetchItem(context, `projects/${encodeURIComponent(publicProjectPath)}`)

  // Synchronize labels.
  console.log("Synchronizing labels...")
  const internalLabelByName = {}
  const internalLabelsPath = `projects/${internalProject.id}/labels`
  for (let internalLabel of await fetchItems(context, internalLabelsPath)) {
    internalLabelByName[internalLabel.name] = internalLabel
  }
  for (let publicLabel of await fetchItems(context, `projects/${publicProject.id}/labels`)) {
    let internalLabel = internalLabelByName[publicLabel.name]
    if (!internalLabel) {
      console.log(`Creating label ${JSON.stringify(publicLabel, null, 2)}.`)
      internalLabel = await createItem(context, internalLabelsPath, publicLabel)
      internalLabelByName[internalLabel.name] = internalLabel
    } else if (!lodash.matches(internalLabel)(publicLabel)) {
      console.log(`Updating label ${JSON.stringify(internalLabel, null, 2)} to` +
        ` ${JSON.stringify(publicLabel, null, 2)}.`)
      internalLabel = await updateItem(context, internalLabelsPath, publicLabel)
      internalLabelByName[internalLabel.name] = internalLabel
    }
  }

  // Synchronize milestones.
  console.log("Synchronizing milestones...")
  const ignoredMilestoneAttributes = ["created_at", "id", "iid", "project_id", "updated_at"]
  const internalMilestoneByTitle = {}
  const internalMilestoneIdByPublicId = {}
  const internalMilestonesPath = `projects/${internalProject.id}/milestones`
  for (let internalMilestone of await fetchItems(context, internalMilestonesPath)) {
    internalMilestoneByTitle[internalMilestone.title] = internalMilestone
  }
  for (let publicMilestone of await fetchItems(context, `projects/${publicProject.id}/milestones`)) {
    let internalMilestone = internalMilestoneByTitle[publicMilestone.title]
    if (!internalMilestone) {
      if (publicMilestone.state === "active") {
        delete publicMilestone.state
        const publicMilestoneCore = cleanupItem(publicMilestone, ignoredMilestoneAttributes)
        console.log(`Creating milestone ${JSON.stringify(publicMilestoneCore, null, 2)}.`)
        internalMilestone = await createItem(context, internalMilestonesPath, publicMilestoneCore)
        internalMilestoneByTitle[internalMilestone.title] = internalMilestone
      }
    } else {
      const internalMilestoneCore = cleanupItem(internalMilestone, ignoredMilestoneAttributes)
      const publicMilestoneCore = cleanupItem(publicMilestone, ignoredMilestoneAttributes)
      if (!lodash.matches(internalMilestoneCore)(publicMilestoneCore)) {
        console.log(`Updating milestone ${JSON.stringify(internalMilestoneCore, null, 2)} to` +
          ` ${JSON.stringify(publicMilestoneCore, null, 2)}.`)
        if (internalMilestone.state !== publicMilestone.state) {
          publicMilestoneCore.state_event = {
            active: "activate",
            closed: "close",
          }[publicMilestone.state]
        }
        delete publicMilestoneCore.state
        internalMilestone = await updateItem(context, `${internalMilestonesPath}/${internalMilestone.id}`,
          publicMilestoneCore)
        internalMilestoneByTitle[internalMilestone.title] = internalMilestone
      }
    }
    if (internalMilestone) internalMilestoneIdByPublicId[publicMilestone.id] = internalMilestone.id
  }

  // Synchronize issues.
  console.log("Synchronizing issues...")
  const ignoredIssueAttributes = ["created_at", "id", "iid", "project_id", "updated_at"]
  const internalIssueByPublicIid = {}
  const internalIssueByTitle = {}
  const internalIssueIdByPublicId = {}
  const internalIssuesPath = `projects/${internalProject.id}/issues`
  for (let internalIssue of await fetchItems(context, internalIssuesPath)) {
    internalIssueByTitle[internalIssue.title] = internalIssue
    let description = internalIssue.description
    if (description) {
     let match = description.match(/^Ticket public etalab\/suivi#(\d+)/)
     if (match) {
       let publicIssueIid = parseInt(match[1])
       internalIssueByPublicIid[publicIssueIid] = internalIssue
     }
    }
  }
  const publicIssueByTitle = {}
  for (let publicIssue of await fetchItems(context, `projects/${publicProject.id}/issues`)) {
    let existingIssue = publicIssueByTitle[publicIssue.title]
    if (!existingIssue) {
      publicIssueByTitle[publicIssue.title] = publicIssue
    } else if (publicIssue.state !== "closed") {
      assert.equal(existingIssue.state, "closed", `Duplicate open ticket: ${publicIssue.title}`)
      publicIssueByTitle[publicIssue.title] = publicIssue
    }
  }
  for (let title in publicIssueByTitle) {
    let publicIssue = publicIssueByTitle[title]
    publicIssue.description = `Ticket public etalab/suivi#${publicIssue.iid}, créé par` +
      ` @${publicIssue.author.username}\n\n` + (publicIssue.description || "")
    if (publicIssue.assignee) {
      publicIssue.assignee_id = publicIssue.assignee.id
    }
    delete publicIssue.assignee
    delete publicIssue.author
    if (publicIssue.labels) {
      publicIssue.labels = publicIssue.labels.join(",")
    }
    if (publicIssue.milestone) {
      publicIssue.milestone_id = internalMilestoneIdByPublicId[publicIssue.milestone.id]
    }
    delete publicIssue.milestone
    if (publicIssue.state === "reopened") publicIssue.state = "opened"

    let internalIssue = internalIssueByPublicIid[publicIssue.iid]
    if (!internalIssue) internalIssue = internalIssueByTitle[publicIssue.title]
    if (!internalIssue) {
      if (publicIssue.state === "opened") {
        delete publicIssue.state
        const publicIssueCore = cleanupItem(publicIssue, ignoredIssueAttributes)
        console.log(`Creating issue ${JSON.stringify(publicIssueCore, null, 2)}.`)
        internalIssue = await createItem(context, internalIssuesPath, publicIssueCore)
        internalIssueByTitle[internalIssue.title] = internalIssue
      }
    } else {
      if (internalIssue.assignee) {
        internalIssue.assignee_id = internalIssue.assignee.id
      }
      delete internalIssue.assignee
      delete internalIssue.author
      if (internalIssue.milestone) {
        internalIssue.milestone_id = internalIssue.milestone.id
      }
      if (internalIssue.labels) {
        internalIssue.labels = internalIssue.labels.join(",")
      }
      delete internalIssue.milestone
      if (internalIssue.state === "reopened") internalIssue.state = "opened"

      const internalIssueCore = cleanupItem(internalIssue, ignoredIssueAttributes)
      const publicIssueCore = cleanupItem(publicIssue, ignoredIssueAttributes)
      if (!lodash.matches(internalIssueCore)(publicIssueCore)) {
        console.log(`Updating issue ${JSON.stringify(internalIssueCore, null, 2)} to` +
          ` ${JSON.stringify(publicIssueCore, null, 2)}.`)
        if (internalIssue.state !== publicIssue.state) {
          publicIssueCore.state_event = {
            closed: "close",
            opened: "reopen",
            reopened: "reopen",
          }[publicIssue.state]
        }
        delete publicIssueCore.state
        internalIssue = await updateItem(context, `${internalIssuesPath}/${internalIssue.id}`, publicIssueCore)
        internalIssueByTitle[internalIssue.title] = internalIssue
      }
    }
    if (internalIssue) internalIssueIdByPublicId[publicIssue.id] = internalIssue.id
  }

  // Synchronize notes of issues
  console.log("Synchronizing notes of issues...")
  const ignoredNoteAttributes = ["created_at", "downvote", "id", "issue_id", "project_id", "system", "updated_at",
    "upvote"]
  for (let publicIssueId in internalIssueIdByPublicId) {
    const internalIssueId = internalIssueIdByPublicId[publicIssueId]
    const internalNoteByBody = {}
    const internalNotesPath = `projects/${internalProject.id}/issues/${internalIssueId}/notes`
    for (let internalNote of await fetchItems(context, internalNotesPath)) {
      internalNoteByBody[internalNote.body] = internalNote
    }
    for (let publicNote of await fetchItems(context, `projects/${publicProject.id}/issues/${publicIssueId}/notes`)) {
      if (publicNote.system) continue
      publicNote.body = `Note publique créée par` +
        ` @${publicNote.author.username}\n\n` + (publicNote.body || "")
      delete publicNote.author

      let internalNote = internalNoteByBody[publicNote.body]
      if (!internalNote) {
        const publicNoteCore = cleanupItem(publicNote, ignoredNoteAttributes)
        console.log(`Creating note ${JSON.stringify(publicNoteCore, null, 2)}.`)
        internalNote = await createItem(context, internalNotesPath, publicNoteCore)
        internalNoteByBody[internalNote.body] = internalNote
      } else {
        delete internalNote.author

        const internalNoteCore = cleanupItem(internalNote, ignoredNoteAttributes)
        const publicNoteCore = cleanupItem(publicNote, ignoredNoteAttributes)
        if (!lodash.matches(internalNoteCore)(publicNoteCore)) {
          console.log(`Updating note ${JSON.stringify(internalNoteCore, null, 2)} to` +
            ` ${JSON.stringify(publicNoteCore, null, 2)}.`)
          internalNote = await updateItem(context, `${internalNotesPath}/${internalNote.id}`, publicNoteCore)
          internalNoteByBody[internalNote.body] = internalNote
        }
      }
    }
  }
}


main()
  .catch(error => console.log(error.stack))
